TUMIKI Fighters
===============

**Tumiki Fighters** created by Kenta Cho, gives a different touch to the side
scrolling shooting genre. With simple 3D graphics, the player has to control
a ship and shoot down enemies. The player can enter and trap the wreckage
in its own hull for bonus points and protection from enemy attacks.

**How to play**
---------------
Control your ship and destroy enemies. The ship is destroyed when it
is hit by a bullet. The enemy's body has no collision damage.

You can pick up the broken pieces of the enemy. The pieces are attached
to your ship and counter the enemies. You can also earn the bonus score
by keeping many pieces stuck. Trapped pieces are destroyed when they touch
the enemy's bullet.

While pressing a slow key, the ship is slow and the ship's direction is fixed.
Trapped pieces are pulled and you can prevent an accident with them,
but the bonus score drops to a fifth. The enemy's pieces are not stuck while
pressing this key.

**Licenses**
------------

> **Tumiki Fighters** was created by [Kenta Cho](http://www.asahi-net.or.jp/~cs8k-cyu/windows/tf_e.html)
> and released with BSD 2-Clause License that can be found in the [readme.txt](readme.txt) and [readme_e.txt](readme_e.txt).

* Copyright (c) 2004-2018 Kenta Cho (ABA Games)
* Copyright (c) 2020 Individual work by Carlos Donizete Froes [a.k.a coringao]
